## Empathy Totems

> The world's first smart totem to track, analyze and represent emotion data.  
> Let's improve the quality of human lifeform.

##### Content

- Struggles of an everyday life
- The product
- Product features
- Photos
- About

##### Struggles of an everyday life

<!-- [Quotes, paradoxes and existentialist goodies here] -->

Depression affects about 121 million people worldwide, with many more going undiagnosed, and is the leading cause of disability. Anxiety disorders touch 16% of people globally at point in their lives. [Ashford]


##### The Product

The world's first smart totem to track, analyze and represent emotion data.

- First totem with human emotion rate monitoring
- High accuracy happiness tracking based on the Experience Sampling Method (ESM)
- Free app to monitor, visualize, and understand your emotional patterns
- Automatic upload readings over Bluetooth


The human emotion tracking totem

- Emotions and feelings tracking
- Empathy levels measurement and analysis
- Openness in communication channels with loved ones
- Wireless sync via Bluetooth

##### Product Features

- Data input mobile app for constant and accurate data gathering.
- Data entry implementation based on the Experience Sampling Method (ESM), an electronic paging approach to study happiness. [Hektner, Csikszentmihalyi]
- Automatic data synchronization. Constant and up-to-date empathy rate visual representation.
- Seamless connectivity. Bluetooth and Wi-Fi for easy setup and hassle-free data transfers. [coming soon]

##### Photos

[coming soon]

##### About

[Empathy Totems Design Document &rarr;](Design-Document.md)

- - -

The Empathy Totem system was design by Luis Cielak for happier humans :)









